# Code Chat

## Task Aim
The aim of this project is to set up a tool (assistant) for writing code using Large Language Models (LLMs).

This code-chat assistant is an additional part of a recruitment task. As the initial approach to solve this task, a small app was created to interact with a chosen LLM model and discuss coding tasks.

For this purpose, the "qwen1_5-0_5b-chat-q8_0.gguf" model was selected due to its surprisingly small size, as mentioned in the llama-cpp-python library.

In this project, llama-cpp-python was chosen for instantiation and communication with the LLM. This library provides Python bindings for the llama.cpp library.

The python-llama-cpp library allows us to create an instance of the LLM locally with proper setup for using both CPU and GPU resources. The LLM instance created with the Llma class has two important parameters that allow us to decide how many GPU resources we want to utilize during LLM calculations:
- `n_gpu_layers`: Specifies the number of GPU layers.
- `main_gpu`: Specifies the main GPU to use.

These parameters are set to 0 by default, so if we want to run small quantized models or if we don't have a proper GPU, we don't need to worry about them. They control how many calculations from the LLM model will be passed to the GPU.

## Code-Chat Components

### 1st - Backend
The backend includes the `ChatModel` class, which facilitates downloading, caching, and creating an instance of the LLM model. This enables interaction with the LLM model in chat mode. In this mode, prompts are passed into the model as a set of assistant and user messages. The assistant message contains a system message with a set of hints for the assistant, which can change the way the assistant responds to user prompts. Each model instance has its own message history to maintain the conversation context. However, this history can become too long, resulting in significantly slower responses from the model. Therefore, a mechanism is needed to maintain a manageable context history, such as through summarization.

The `ChatModel` exposes significant LLM parameters that control its output, including:
- `temperature`: Controls the creativity of the model.
- `top_p`: Allows the model to dynamically choose the optimal number of words.
- `top_k`: Dictates the number of words from which the model will sample the next word.
- `max_tokens`: Specifies the maximum number of tokens to generate.
- `frequency_penalty`: Applies a penalty to tokens based on their frequency in the prompt.
- `repeat_penalty`: Ensures that the model doesn’t output duplicate text.

### 2nd - User Interface
The user interface is created using the [Streamlit](https://streamlit.io/) Python library, which allows building clean and simple user interfaces from code blocks.

## How to Run
To run the code chat:
1. Clone the repository [code-chat](https://gitlab.com/lmm-custom-copilot/code-chat).
2. Create a Python virtual environment (tested with Python 3.12).
3. Install the required dependencies:
   ```
   pip install -r requirements.txt
   ```
4. Run the application:
   ```
   streamlit run app.py
   ```
   After that, the chat app will be available locally at: [http://localhost:8502](http://localhost:8502)

---
