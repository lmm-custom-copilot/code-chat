from llama_cpp import Llama
from llama_cpp.llama_tokenizer import  LlamaHFTokenizer
import gradio as gr


"""
 Create the LLM instantion
"""

class ChatModel:
    """
    Create the LLM instantion
    by default llama cpp use only cpu. To involve GPU 
    we need to change default values (0) according to our needs:
    n_gpu_layers: int = 0 
    main_gpu: int = 0
    """
    def __init__(self):
        self.model = Llama.from_pretrained(
            repo_id="Qwen/Qwen1.5-0.5B-Chat-GGUF",
            filename="*q8_0.gguf",
            tokenizer=LlamaHFTokenizer.from_pretrained("Qwen/Qwen1.5-0.5B"),
            verbose=False,
            cache_dir="./models"
        )

        self.history=[]
        self.DEFAULT_SYSTEM_PROMPT = """\
        You are a helpful, respectful and honest assistant with a deep knowledge of code and software design. Always answer as helpfully as possible, while being safe. Your answers should not include any harmful, unethical, racist, sexist, toxic, dangerous, or illegal content. Please ensure that your responses are socially unbiased and positive in nature.\n\nIf a question does not make any sense, or is not factually coherent, explain why instead of answering something not correct. If you don't know the answer to a question, please don't share false information.\
        """

    def generate(self, user_prompt, system_prompt, top_p=0.95, temperature=0.2, max_tokens=512, repeat_penalty=1.1, frequency_penalty=0):
        """Generate a chat completion from a list of messages.

        Args:
            messages: A list of messages to generate a response for.
            temperature: The temperature to use for sampling.
            top_p: The top-p value to use for nucleus sampling. Nucleus sampling described in academic paper "The Curious Case of Neural Text Degeneration" https://arxiv.org/abs/1904.09751
            top_k: The top-k value to use for sampling. Top-K sampling described in academic paper "The Curious Case of Neural Text Degeneration" https://arxiv.org/abs/1904.09751
            stream: Whether to stream the results
            max_tokens: The maximum number of tokens to generate. If max_tokens <= 0 or None, the maximum number of tokens to generate is unlimited and depends on n_ctx.
            frequency_penalty: The penalty to apply to tokens based on their frequency in the prompt.
            repeat_penalty: The penalty to apply to repeated tokens.
            model: The name to use for the model in the completion object.
        Returns:
            Generated chat completion or a stream of chat completion chunks.
        """
        messages=[{"role": "assistant", "content": system_prompt}]
        for item in self.history:
            messages.append(item)
            
        messages.append({"role": "user", "content": user_prompt})
        self.history.append({"role": "user", "content": user_prompt})

        response = self.model.create_chat_completion_openai_v1(
        model="gpt-3.5-turbo",
        messages=messages,
        stream=True,
        max_tokens=max_tokens,
        top_k=40,
        top_p=top_p,
        temperature=temperature,
        repeat_penalty=repeat_penalty

    )

        text = ""
        for chunk in response:
            content = chunk.choices[0].delta.content
            if content:
                text += content
                #TODO:investigate why yeld caused a lot of content repetition (onbly in graphical interface)
                #yield text
        return text

    